package org.mobous.controller{
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	import org.mobous.event.HerokuEvent;
	import org.mobous.event.UserEvent;
	import org.mobous.model.App;
	import org.mobous.model.User;
	import org.mobous.service.HaikuService;
	import org.mobous.service.HerokuService;
	import org.swizframework.utils.services.ServiceHelper;

	public class UserController{
		[Inject]
		public var herokuService:HerokuService;
		[Inject]
		public var haikuService:HaikuService;
		[Inject]
		public var serviceHelper:ServiceHelper;
		[Bindable]
		public var currentUser:User;
		[Dispatcher]
		public var dispatcher:IEventDispatcher;
		
		[PostConstruct]
		public function createDefaultUser():void{
			currentUser = new User();
			if(haikuService.isAuthenticated()){
				// populate the user
				var auth:String = haikuService.getAuthentication();
				currentUser.populate(auth);
			}else currentUser = new User();
		}
		
		[EventHandler(event="UserEvent.AUTHENTICATE_USER", properties="user")]
		public function authenticateUser(user:User):void{
			currentUser = user;
			serviceHelper.executeServiceCall(herokuService.authenticate(user), handleUserAuthResult);
		}
		
		[EventHandler(event="HerokuEvent.GET_APPS", properties="user")]
		public function getApps(user:User):void{
			serviceHelper.executeServiceCall(herokuService.apps(user), handleAppsResult);
		}
		
		private function handleUserAuthResult(event:ResultEvent):void{
			var herokuEvent:HerokuEvent;
			if(event.statusCode == 200){
				haikuService.setAuthentication(currentUser.getAuthString());
				currentUser.apps = new Array();
				parseApps(event.result.apps.app);
				herokuEvent = new HerokuEvent(HerokuEvent.CALL_SUCCESSFUL);
			} else {
				herokuEvent = new HerokuEvent(HerokuEvent.CALL_FAILED);
			}
			herokuEvent.endpoint = 'auth';
			dispatcher.dispatchEvent(herokuEvent);
		}
		
		public function handleAppsResult(event:ResultEvent):void{
			var herokuEvent:HerokuEvent;
			if(event.statusCode == 200){
				currentUser.apps = new Array();
				parseApps(event.result.apps.app);
				herokuEvent = new HerokuEvent(HerokuEvent.CALL_SUCCESSFUL);
			}else{
				herokuEvent = new HerokuEvent(HerokuEvent.CALL_FAILED);
			}
			herokuEvent.endpoint = 'apps';
			dispatcher.dispatchEvent(herokuEvent);
		}
		
		public function parseApps(apps:ArrayCollection):void{
			for(var i:Number=0; i<apps.length; i++){
				var app:App = new App();
				app.createFromRaw(apps[i]);
				currentUser.apps.push(app);
			}
		}
	}
}