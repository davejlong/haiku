package org.mobous.service{
	import air.net.URLMonitor;
	
	import flash.events.StatusEvent;
	import flash.net.URLRequest;
	
	import mx.utils.Base64Decoder;
	
	import spark.managers.PersistenceManager;

	public class HaikuService{
		private var pm:PersistenceManager = new PersistenceManager;
		public function HaikuService(){
			pm.load();
		}
		
		public function setAuthentication(auth:String):void{
			pm.setProperty('authentication',auth);
		}
		public function getAuthentication():String{
			var auth:Object = pm.getProperty('authentication');
			
			if(auth != null){
				return auth.toString();
			}else return '';
		}
		public function isAuthenticated():Boolean{
			var auth:Object = pm.getProperty('authentication');
			if(auth != null && auth.toString().length) return true;
			else return false;
			return true;
		}
	}
}