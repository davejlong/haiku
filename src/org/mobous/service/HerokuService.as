package org.mobous.service{
	import flash.events.IEventDispatcher;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.http.HTTPService;
	
	import org.mobous.event.HerokuEvent;
	import org.mobous.event.UserEvent;
	import org.mobous.model.User;

	public class HerokuService{
		[Dispatcher]
		public var dispatcher:IEventDispatcher;		
		
		public function HerokuService(){
		}
		
		public function authenticate(user:User):AsyncToken{
			// Since authenticate is the same as apps just use that
			return apps(user);
		}
		
		public function apps(user:User):AsyncToken{
			var call:HTTPService = createBaseAPICall(user);
			call.url = '/apps';
			return call.send();
		}
		
		private function createBaseAPICall(user:User):HTTPService{
			var apicall:HTTPService = new HTTPService('http://api.heroku.com');
			apicall.method = 'GET'; // Default API method
			apicall.headers = {
				Authorization:"Basic " + user.getAuthString(),
				Accept:"application/xml"
			};
			
			apicall.addEventListener(FaultEvent.FAULT, function(event:FaultEvent):void{
				var herokuEvent:HerokuEvent = new HerokuEvent(HerokuEvent.CALL_FAILED);
				herokuEvent.error = event.fault.content.toString().replace(/(HTTP\ Basic\:)\ /i);
				dispatcher.dispatchEvent(herokuEvent);
			});
			return apicall;
		}
	}
}