package org.mobous.model
{
	import mx.formatters.DateBase;

	[Bindable]
	public class App
	{
		public var id:String;
		public var createdAt:Date;
		public var domainName:String;
		public var dynos:Number;
		public var gitURL:String;
		public var name:String;
		public var owner:String;
		public var repoSize:Number;
		public var stack:String;
		public var webURL:String;
		public var workers:Number;
		
		public function createFromRaw(app:Object):void{
			id = app['id'];
			domainName = app['domain_name'];
			dynos = new Number(app['dynos'].value);
			gitURL = app['git_url'];
			name = app['name'];
			owner = app['owner'];
			repoSize = new Number(app['repo-size'].value);
			stack = app['stack'];
			webURL = app['web_url'].toString().replace(/http(s?):\/\/|\//gi,'');
			workers = new Number(app['workers'].value);
			
			// This is the only one that is difficult.  AS appearantly sucks with dates
			var matches : Array = app['created-at'].value.match(/(\d\d\d\d)-(\d\d)-(\d\d)/);
			var d : Date = new Date();
			d.setUTCFullYear(int(matches[1]), int(matches[2]) - 1, int(matches[3]));
			createdAt = d;
		}
	}
}