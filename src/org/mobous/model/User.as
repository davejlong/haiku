package org.mobous.model
{
	import com.hurlant.util.Base64;
	
	
	
	[Bindable]
	public class User
	{
		public var email:String;
		public var password:String;
		public var apps:Array;
		
		public function getAuthString():String{
			var encoded:String = Base64.encode(email + ':' + password);			
			return encoded;
		}
		
		public function populate(auth:String):void{
			var decoded:String = Base64.decode(auth);
			var colon:Number = decoded.search(/\:/);
			email = decoded.substring(0, colon);
			password = decoded.substr(colon+1);			
		}
	}
}