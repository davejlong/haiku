package org.mobous.event
{
	import flash.events.Event;
	
	import org.mobous.model.User;
	
	public class UserEvent extends Event
	{
		public static const AUTHENTICATE_USER:String = 'authenticateUser';
		
		public var user:User;
		public function UserEvent(type:String)
		{
			super(type, true);
		}
	}
}