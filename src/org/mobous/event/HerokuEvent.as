package org.mobous.event
{
	import flash.events.Event;
	
	import org.mobous.model.User;
	
	public class HerokuEvent extends Event
	{
		public static const GET_APPS:String = 'getApps';
		public static const CALL_SUCCESSFUL:String = 'callSuccessful';
		public static const CALL_FAILED:String = 'callFailed';
		
		public var endpoint:String;
		
		public var user:User;
		public var error:String = '';
		public function HerokuEvent(type:String){
			super(type, true);
		}
	}
}